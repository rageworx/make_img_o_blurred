#include <FL/Fl.H>
#include <FL/Fl_Tooltip.H>
#include <FL/fl_ask.H>
#include "fl_imgtk.h"

#include <string>

#if ( FL_ABI_VERSION < 10304 )
    #error "Error, FLTK ABI need 1.3.4 or above"
#endif

#if ( FL_IMGTK_VER_BLD < 26 )
    #error "Error, Need FL_IMGTK better build ( 0.3.26.x )"
#endif

#include "winMain.h"

using namespace std;

////////////////////////////////////////////////////////////////////////////////

#define DEF_APP_CLSNAME         "mkimgoblur"

////////////////////////////////////////////////////////////////////////////////

const char* convLoc = NULL;
const char* convLng = NULL;

char* argv_me_path = NULL;
char* argv_me_bin  = NULL;

////////////////////////////////////////////////////////////////////////////////

void procAutoLocale()
{
    LANGID currentUIL = GetSystemDefaultLangID();

    switch( currentUIL & 0xFF )
    {
        case LANG_KOREAN:
            convLoc = "Korean_Korea.949";
            convLng = "Malgun Gothic";
            break;

        case LANG_JAPANESE:
            convLoc = "Japanese_Japan.932";
            convLng = "Meiryo";
            break;

        case LANG_CHINESE:
            convLoc = "Chinese_China.936";
            convLng = "Microsoft YaHei";
            break;

        case LANG_CHINESE_TRADITIONAL:
            convLoc = "Chinese (Traditional)_Taiwan.950";
            convLng = "Microsoft JhengHei";
            break;

        default:
            convLoc = "C";
            convLng = "Tahoma";
            break;
    }

    setlocale( LC_ALL, convLoc );
}

void parserArgvZero( const char* argv0 )
{
    string argvextractor = argv0;

#ifdef _WIN32
    char splitter[] = "\\";
#else
    char splitter[] = "/";
#endif

    if ( argvextractor.size() > 0 )
    {
        string::size_type lastSplitPos = argvextractor.rfind( splitter );

        string extracted_path = argvextractor.substr(0, lastSplitPos + 1 );
        string extracted_name = argvextractor.substr( lastSplitPos + 1 );

        argv_me_path = strdup( extracted_path.c_str() );
        argv_me_bin  = strdup( extracted_name.c_str() );
    }
}

void presetFLTKenv()
{
    Fl::set_font( FL_FREE_FONT, convLng );
    Fl_Double_Window::default_xclass( DEF_APP_CLSNAME );

    fl_message_font_ = FL_FREE_FONT;
    fl_message_size_ = 11;

    Fl_Tooltip::color( fl_darker( FL_DARK3 ) );
    Fl_Tooltip::textcolor( FL_WHITE );
    Fl_Tooltip::size( 11 );
    Fl_Tooltip::font( FL_FREE_FONT );

    Fl::scheme( "flat" );
}

int main (int argc, char ** argv)
{
    int reti = 0;

    parserArgvZero( argv[0] );

    presetFLTKenv();

    WMain* pWMain = new WMain( argc, argv );

    if ( pWMain != NULL )
    {
        procAutoLocale();

        reti = pWMain->Run();
    }

    if ( argv_me_path != NULL )
    {
        free( argv_me_path );
    }

    if ( argv_me_bin != NULL )
    {
        free( argv_me_bin );
    }

    return reti;
}
