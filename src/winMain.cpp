#include <windows.h>

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>

#include "winMain.h"
#include "fl_imgtk.h"

#include <pthread.h>

#include "resource.h"
#include "dtools.h"
#include "ltools.h"

#include <FL/fl_draw.H>
#include <Fl/Fl_BMP_Image.H>
#include <Fl/Fl_PNG_Image.H>
#include <Fl/Fl_JPEG_Image.H>
#include <FL/Fl_Native_File_Chooser.H>

#if defined(USE_SYSPNG)
	#include <png.h>
#else
	#include <FL/images/png.h>
#endif /// of defined(USE_SYSPNG)

////////////////////////////////////////////////////////////////////////////////

using namespace std;

////////////////////////////////////////////////////////////////////////////////

#define DEF_APP_NAME            "Make Image O Blurred"
#define DEF_WIDGET_FSZ          11
#define DEF_WIDGET_FNT          FL_FREE_FONT

#define DEF_WIN_COLOR_BG        0x33333300
#define DEF_WIN_COLOR_FG        0xAAAAAA00

////////////////////////////////////////////////////////////////////////////////

WMain::WMain( int argc, char** argv )
 :  _argc( argc ),
    _argv( argv ),
    crossfade_idx( 0 ),
    crossfade_ratio( 0.0f ),
    window( NULL ),
    imgControlBG( NULL ),
    imgSource( NULL ),
    imgBlurred( NULL )
{
    getEnvironments();
    createComponents();
    startMyImageScan();

#ifdef _WIN32
    HICON \
    hIconWindowLarge = (HICON)LoadImage( fl_display,
                                         MAKEINTRESOURCE( IDC_ICON_A ),
                                         IMAGE_ICON,
                                         64,
                                         64,
                                         LR_SHARED );

    HICON \
    hIconWindowSmall = (HICON)LoadImage( fl_display,
                                         MAKEINTRESOURCE( IDC_ICON_A ),
                                         IMAGE_ICON,
                                         16,
                                         16,
                                         LR_SHARED );

    if ( window != NULL )
    {
        SendMessage( fl_xid( window ),
                     WM_SETICON,
                     ICON_BIG,
                     (LPARAM)hIconWindowLarge );

        SendMessage( fl_xid( window ),
                     WM_SETICON,
                     ICON_SMALL,
                     (LPARAM)hIconWindowSmall );
    }
#endif /// of _WIN32
}


WMain::~WMain()
{
    listBackImages.clear();
}

int WMain::Run()
{
    if ( window != NULL )
    {
        return Fl::run();
    }

    return 0;
}

void WMain::WidgetCall( Fl_Widget* w )
{
    if ( ( w == sliBlur ) || ( w == sliCont ) )
    {
        if ( imgSource == NULL )
            return;

        double bv = sliBlur->value();
        double cv = sliCont->value();

        window->cursor( FL_CURSOR_WAIT );

        Fl::lock();

        if ( imgBlurred != NULL )
        {
            fl_imgtk::discard_user_rgb_image( imgBlurred );
        }

        if ( bv == 0 )
        {
            imgBlurred = (Fl_RGB_Image*)imgSource->copy();
        }
        else
        {
            unsigned bfactor = (unsigned) 2 * ( bv / 2.0 );
            imgBlurred = fl_imgtk::blurredimage( imgSource, bfactor );
        }

        if ( imgBlurred != NULL )
        {
            double acv = ( cv - 50.0 ) * 2.0;
            fl_imgtk::brightness_ex( imgBlurred, acv );

            boxImgPreView->image( imgBlurred, sensFitting( imgBlurred ) );
            boxImgPreView->redraw();
        }

        Fl::unlock();

        window->cursor( FL_CURSOR_DEFAULT );

        return;
    }

    if ( w == btnSave )
    {
        if ( imgBlurred != NULL )
        {
            btnSave->deactivate();
            saveImageProc();
        }
    }
}

void* WMain::PThreadCall()
{
    // Search images in User directory ...

    pthread_exit( NULL );
    return NULL;
}

void WMain::OnMouseMove( void* w, int x, int y, bool inside )
{

}

void WMain::OnMouseClick( void* w, int x, int y, unsigned btn )
{

}

void WMain::OnKeyPressed( void* w, unsigned short k, int s, int c, int a )
{

}

void WMain::OnDropFiles( void* w, char* files )
{
    if ( ( w == boxImgPreView ) && ( files != NULL ) )
    {
        // decode UTF-8 strings to wide characters.

        size_t decodelen = strlen( files );
        if ( decodelen > 0 )
        {
#ifdef SUPPORT_WCHAR
            wchar_t* decodedstr = new wchar_t[ decodelen + 1 ];
            if ( decodedstr != NULL )
            {
                fl_utf8towc( files, strlen( files ),
                             decodedstr, decodelen + 1 );

                wstring tmpfiles = decodedstr;

                delete[] decodedstr;

                vector<wstring> tmpfilelist;

                tmpfilelist = split( tmpfiles, L'\n' );
#else
           const char* decodedstr = files;
           if ( decodedstr != NULL )
           {
                vector<string> tmpfilelist;
                string tmpfiles = decodedstr;

                tmpfilelist = split( tmpfiles, '\n' );
#endif

                if ( tmpfilelist.size() > 0 )
                {
                    for( size_t cnt=0; cnt<tmpfilelist.size(); cnt++ )
                    {
                        // check while image loads.
                        char* tmpbuff = NULL;
                        size_t tmpbuffsz = 0;

                        int ret = testImageFile( tmpfilelist[ cnt ].c_str(), &tmpbuff, &tmpbuffsz );
                        if ( ( ret > 0 ) && ( tmpbuffsz > 0 ) )
                        {
                            Fl_RGB_Image* tmpimg = NULL;

                            switch( ret )
                            {
                                case 1: /// JPEG
                                    tmpimg = new Fl_JPEG_Image( "JPGIMG",
                                                                (const uchar*)tmpbuff );
                                    break;

                                case 2: /// PNG
                                    tmpimg = new Fl_PNG_Image( "PNGIMAGE",
                                                               (const uchar*)tmpbuff, tmpbuffsz );
                                    break;

                                case 3: /// BMP
                                    tmpimg = fl_imgtk::createBMPmemory( (const char*)tmpbuff,
                                                                        tmpbuffsz );
                                    break;
                            }

                            if ( tmpimg != NULL )
                            {
                                fl_imgtk::discard_user_rgb_image( imgBlurred );
                                fl_imgtk::discard_user_rgb_image( imgSource );

                                if ( tmpimg->d() < 3 )
                                {
                                    if ( convImage( tmpimg, imgSource ) == true )
                                    {
                                        fl_imgtk::discard_user_rgb_image( tmpimg );
                                    }
                                    else
                                    {
                                        imgSource = tmpimg;
                                    }
                                }
                                else
                                {
                                    imgSource = tmpimg;
                                }

                                // reset control values ...
                                sliBlur->value( 0 );
                                sliCont->value( 50 );

                                boxImgPreView->image( imgSource, sensFitting( imgSource ) );
                                boxImgPreView->redraw();

                                pathImage = stripFilePath( tmpfilelist[ cnt ].c_str() );
                                imgFName  = stripFileName( tmpfilelist[ cnt ].c_str() );

                                // Convert source image to new name to be saved.
#ifdef SUPPORT_WCHAR
                                wstring tmpwstr = imgFName;
                                size_t fpos = tmpwstr.find_last_of( L'.' );
                                if ( fpos != string::npos )
                                {
                                    tmpwstr = tmpwstr.substr( 0, fpos );
                                }
                                char tmputf8[1024] = {0,};
                                fl_utf8fromwc( tmputf8, 1024, tmpwstr.c_str(), tmpwstr.size() );
                                imgFNameUTF8 = tmputf8;
#else
                                string tmpstr = imgFName;
                                size_t fpos = tmpstr.find_last_of( '.' );
                                if ( fpos != string::npos )
                                {
                                    tmpstr = tmpstr.substr( 0, fpos );
                                }
                                imgFNameUTF8 = tmpstr;
#endif
                                // Update window title.
                                setdefaultwintitle();
                                string tmpwintitle = wintitlestr;
                                sprintf( wintitlestr,
                                         "%s : %s",
                                         imgFNameUTF8.c_str(),
                                         tmpwintitle.c_str() );
                                window->label( wintitlestr );

                                return;
                            }
                        }
                    }
                } /// of if ( tmpfilelist.size() > 0 )
            }
        }
    }
}

void WMain::OnDrawCompleted()
{
    boxControl->hide();
    boxControl->deimage();
    //boxControl->image( NULL );

    if ( imgControlBG != NULL )
    {
        fl_imgtk::discard_user_rgb_image( imgControlBG );
        //delete imgControlBG;
    }

    uchar* widgetbuff = new uchar[ boxControl->w() * boxControl->h() * 3 + 1];
    if ( widgetbuff != NULL )
    {
        // Read current window pixels to buffer for create a new Fl_RGB_Image.
        fl_read_image( widgetbuff, boxControl->x(), boxControl->y(),
                                   boxControl->w(), boxControl->h() );

        Fl_RGB_Image* imgcntl = new Fl_RGB_Image( widgetbuff,
                                                  boxControl->w(),
                                                  boxControl->h(),
                                                  3 );

        if ( imgcntl != NULL )
        {
            fl_imgtk::brightness_ex( imgcntl, -50 );
            fl_imgtk::draw_smooth_line( imgcntl,
                                        0, 0,
                                        boxControl->w(), 0,
                                        0xFFFFFFFF );
            fl_imgtk::draw_smooth_line( imgcntl,
                                        0, 2,
                                        boxControl->w(), 2,
                                        0xFFFFFFFF );
            fl_imgtk::draw_smooth_line( imgcntl,
                                        0, boxControl->h()-1,
                                        boxControl->w(), boxControl->h()-1,
                                        0xFFFFFFFF );
            fl_imgtk::blurredimage_ex( imgcntl, 8 );
            fl_imgtk::draw_smooth_line( imgcntl,
                                        0, boxControl->h()-1,
                                        boxControl->w(), boxControl->h()-1,
                                        0xFFFFFF1F );

            //imgControlBG = (Fl_RGB_Image*)imgcntl->copy();
            imgControlBG = imgcntl;
            //fl_imgtk::discard_user_rgb_image( imgcntl );

            boxControl->image( imgControlBG );
        }
        else
        {
            delete[] widgetbuff;
        }
    }

    boxControl->show();
}

void WMain::getEnvironments()
{
    pathHome        = _wgetenv( L"USERPROFILE" );
    pathSystemBase  = _wgetenv( L"SYSTEMROOT" );
    pathUserData    = _wgetenv( L"LOCALAPPDATA" );
    pathUserRoaming = _wgetenv( L"APPDATA" );
#ifdef DEBUG
    wprintf( L"pathHome        : %S\n", pathHome.c_str() );
    wprintf( L"pathSystemBase  : %S\n", pathSystemBase.c_str() );
    wprintf( L"pathUserData    : %S\n", pathUserData.c_str() );
    wprintf( L"pathUserRoaming : %S\n", pathUserRoaming.c_str() );
#endif // DEBUG
}

void WMain::setdefaultwintitle()
{
    memset( wintitlestr, 0, MAX_WINTITLE_LEN );
    sprintf( wintitlestr, "%s v.%s", DEF_APP_NAME, APP_VERSION_STR );
}

void WMain::createComponents()
{
    window = new Fl_Double_Window( 640, 440, DEF_APP_NAME );
    if ( window != NULL )
    {
        window->color( DEF_WIN_COLOR_BG );
        window->labelfont( DEF_WIDGET_FNT );
        window->labelsize( DEF_WIDGET_FSZ );
        window->labelcolor( DEF_WIN_COLOR_FG );

        // continue to child components ...
        window->begin();

            boxImgPreView = new Fl_ImageViewer( 0, 0, window->w(), window->h() );
            if ( boxImgPreView != NULL )
            {
                boxImgPreView->notifier( this );
                boxImgPreView->box( FL_NO_BOX );
            }

            grpControl = new Fl_Group( 0, 0, window->w(), 50 );

            if ( grpControl != NULL )
            {
                grpControl->begin();

                boxControl = new Fl_Box( grpControl->x(), grpControl->y(),
                                         grpControl->w(), grpControl->h() );
                if ( boxControl != NULL )
                {
                    boxControl->box( FL_NO_BOX );
                }

                int s_h = 15;
                int s_x = 50;
                int s_y = 5;
                int s_w = ( window->w() / 2 ) - s_x;

                sliBlur = new Fl_Hor_Fill_Slider( s_x, s_y, s_w, s_h, "Blur :" );
                if ( sliBlur != NULL )
                {
                    sliBlur->align( FL_ALIGN_LEFT );
                    sliBlur->labelfont( window->labelfont() );
                    sliBlur->labelsize( window->labelsize() );
                    sliBlur->labelcolor( window->labelcolor() );
                    sliBlur->color( 0x554433FF, 0x887766FF );
                    sliBlur->range( 0, 100 );
                    sliBlur->value( 0 );
                    sliBlur->clear_visible_focus();

                    sliBlur->callback( WMain::WidgetCB, this );
                }

                s_x += s_w + 5 + 70;
                s_w  = ( window->w() / 2 ) - 80;

                sliCont = new Fl_Hor_Fill_Slider( s_x, s_y, s_w, s_h, "Brightness :" );
                if ( sliCont != NULL )
                {
                    sliCont->align( FL_ALIGN_LEFT );
                    sliCont->labelfont( window->labelfont() );
                    sliCont->labelsize( window->labelsize() );
                    sliCont->labelcolor( window->labelcolor() );
                    sliCont->color( 0x554433FF, 0x887766FF );
                    sliCont->range( 0, 100 );
                    sliCont->value( 50 );
                    sliCont->clear_visible_focus();

                    sliCont->callback( WMain::WidgetCB, this );
                }

                s_x  = 5;
                s_w  = window->w() - 10;
                s_y += s_h + 10;

                btnSave = new Fl_Button( s_x, s_y, s_w, s_h, "Save" );
                if ( btnSave != NULL )
                {
                    btnSave->box( FL_THIN_DOWN_BOX );
                    btnSave->labelfont( window->labelfont() );
                    btnSave->labelsize( window->labelsize() );
                    btnSave->labelcolor( window->labelcolor() );
                    btnSave->color( 0x554433FF, 0x887766FF );
                    btnSave->clear_visible_focus();

                    btnSave->callback( WMain::WidgetCB, this );
                }

                grpControl->end();
            }

            grpImageView = new Fl_Group( 0, 50, window->w(), window->h() - 50 );

            if ( grpImageView != NULL )
            {
                grpImageView->begin();

                // Something here ...

                grpImageView->end();
                grpImageView->hide();
            }

        window->end();

        if ( grpImageView != NULL )
        {
            window->resizable( grpImageView );
            window->size_range( window->w(), window->h() );
        }

        window->show();
    }
}

void WMain::startMyImageScan()
{
    pthread_t ptt;
    pthread_create( &ptt, NULL, WMain::PThreadCB, this );
}

void WMain::updateCrossFadeBG()
{
    // Check current cross-fading condition ...

}

int WMain::sensFitting( const Fl_RGB_Image* img )
{
    // Makes current image to where fit
    if ( img != NULL )
    {
        if ( img->w() > img->h() )
        {
            return 0;
        }
        else
        if ( img->w() < img->h() )
        {
            return 1;
        }
    }

    return 0;
}

int WMain::testImageFile( const wchar_t* imgfp, char** buff,size_t* buffsz )
{
    int reti = -1;

    if ( imgfp != NULL )
    {
        FILE* fp = _wfopen( imgfp, L"rb" );
        if ( fp != NULL )
        {
            fseek( fp, 0L, SEEK_END );
            size_t flen = ftell( fp );
            fseek( fp, 0L, SEEK_SET );

            if ( flen > 32 )
            {
                // Test
                char testbuff[32] = {0,};

                fread( testbuff, 1, 32, fp );
                fseek( fp, 0, SEEK_SET );

                const uchar jpghdr[3] = { 0xFF, 0xD8, 0xFF };

                // is JPEG ???
                if( strncmp( &testbuff[0], (const char*)jpghdr, 3 ) == 0 )
                {
                    reti = 1; /// JPEG.
                }
                else
                if( strncmp( &testbuff[1], "PNG", 3 ) == 0 )
                {
                    reti = 2; /// PNG.
                }
                else
                if( strncmp( &testbuff[0], "BM", 2 ) == 0 )
                {
                    reti = 3; /// BMP.
                }

                if ( reti > 0 )
                {
                    *buff = new char[ flen ];
                    if ( *buff != NULL )
                    {
                        fread( *buff, 1, flen, fp );

                        if( buffsz != NULL )
                        {
                            *buffsz = flen;
                        }
                    }
                }
            }

            fclose( fp );
        }
    }

    return reti;
}

int WMain::testImageFile( const char* imgfp, char** buff,size_t* buffsz )
{
    int reti = -1;

    if ( imgfp != NULL )
    {
        FILE* fp = fopen( imgfp, "rb" );
        if ( fp != NULL )
        {
            fseek( fp, 0L, SEEK_END );
            size_t flen = ftell( fp );
            fseek( fp, 0L, SEEK_SET );

            if ( flen > 32 )
            {
                // Test
                char testbuff[32] = {0,};

                fread( testbuff, 1, 32, fp );
                fseek( fp, 0, SEEK_SET );

                const uchar jpghdr[3] = { 0xFF, 0xD8, 0xFF };

                // is JPEG ???
                if( strncmp( &testbuff[0], (const char*)jpghdr, 3 ) == 0 )
                {
                    reti = 1; /// JPEG.
                }
                else
                if( strncmp( &testbuff[1], "PNG", 3 ) == 0 )
                {
                    reti = 2; /// PNG.
                }
                else
                if( strncmp( &testbuff[0], "BM", 2 ) == 0 )
                {
                    reti = 3; /// BMP.
                }

                if ( reti > 0 )
                {
                    *buff = new char[ flen ];
                    if ( *buff != NULL )
                    {
                        fread( *buff, 1, flen, fp );

                        if( buffsz != NULL )
                        {
                            *buffsz = flen;
                        }
                    }
                }
            }

            fclose( fp );
        }
    }

    return reti;
}

bool WMain::convImage( Fl_RGB_Image* src, Fl_RGB_Image* &dst )
{
    if ( src != NULL )
    {
        unsigned img_w = src->w();
        unsigned img_h = src->h();
        unsigned img_d = src->d();
        unsigned imgsz = img_w * img_h;

        uchar*       cdata = NULL;

        switch( img_d )
        {
            case 1: /// single gray
                {
                    const uchar* pdata = (const uchar*)src->data()[0];
                    cdata = new uchar[ imgsz * 3 ];
                    if ( cdata != NULL )
                    {
                        #pragma omp parallel for
                        for( unsigned cnt=0; cnt<imgsz; cnt++ )
                        {
                            cdata[ cnt*3 + 0 ] = pdata[ cnt ];
                            cdata[ cnt*3 + 1 ] = pdata[ cnt ];
                            cdata[ cnt*3 + 2 ] = pdata[ cnt ];
                        }

                        dst = new Fl_RGB_Image( cdata, img_w, img_h, 3 );

                        if ( dst != NULL )
                        {
                            return true;
                        }
                    }
                }
                break;

            case 2: /// Must be RGB565
                {
                    const unsigned short* pdata = (const unsigned short*)src->data()[0];
                    cdata = new uchar[ imgsz * 3 ];
                    if ( cdata != NULL )
                    {
                        #pragma omp parallel for
                        for( unsigned cnt=0; cnt<imgsz; cnt++ )
                        {
                            cdata[ cnt*3 + 0 ] = ( pdata[ cnt ] & 0xF800 ) >> 11;
                            cdata[ cnt*3 + 1 ] = ( pdata[ cnt ] & 0x07E0 ) >> 5;
                            cdata[ cnt*3 + 2 ] = ( pdata[ cnt ] & 0x001F );
                        }

                        dst = new Fl_RGB_Image( cdata, img_w, img_h, 3 );

                        if ( dst != NULL )
                        {
                            return true;
                        }
                    }
                }
                break;

            default:
                {
                    dst = (Fl_RGB_Image*)src->copy();

                    if ( dst != NULL )
                    {
                        return true;
                    }
                }
                break;
        }
    }

    return false;
}

void WMain::saveImageProc()
{
    Fl_Native_File_Chooser nFC;

    static string presetfn;

    presetfn = imgFNameUTF8;
    presetfn += "_blurred.png";

    nFC.title( "Select PNG file to save." );
    nFC.type( Fl_Native_File_Chooser::BROWSE_SAVE_FILE );
    nFC.options( Fl_Native_File_Chooser::USE_FILTER_EXT
                 | Fl_Native_File_Chooser::SAVEAS_CONFIRM );
    nFC.filter( "PNG Image\t*.png" );
    nFC.preset_file( presetfn.c_str() );

    char refconvpath[1024] = {0,};

#ifdef _WIN32
    if ( pathImage.size() > 0 )
    {
        fl_utf8fromwc( refconvpath, 1024,
                       pathImage.c_str(), pathImage.size() );
    }
    else
    {
        fl_utf8fromwc( refconvpath, 1024,
                       pathHome.c_str(), pathHome.size() );
    }
#else
    strcpy( refconvpath, imgFNameUTF8.c_str() );
#endif

    nFC.directory( refconvpath );

    int retVal = nFC.show();

    if ( retVal == 0 )
    {
#ifdef __APPLE__
        Fl::check();
#endif /// of __APPLE__

        imgFNameUTF8 = nFC.directory();

        string savefn = nFC.filename(0);
        string testfn = removeFilePath( savefn.c_str() );

        if ( testfn.find_last_of('.') == string::npos )
        {
            savefn += ".png";
        }

        window->cursor( FL_CURSOR_WAIT );

        save2png( imgBlurred, savefn.c_str() );

        window->cursor( FL_CURSOR_DEFAULT );

        Fl::awake();
    }

    btnSave->activate();
}

bool WMain::save2png( Fl_RGB_Image* img, const char* fname )
{
    if ( ( img != NULL ) && ( fname != NULL ) )
    {
        if ( img->d() < 3 )
            return false;

        if ( access( fname, F_OK ) == 0 )
        {
            if ( unlink( fname ) != 0 )
                return false; /// failed to remove file !
        }

        FILE* fp = fopen( fname, "wb" );
        if ( fp != NULL )
        {
            png_structp png_ptr     = NULL;
            png_infop   info_ptr    = NULL;
            png_bytep   row         = NULL;

            png_ptr = png_create_write_struct( PNG_LIBPNG_VER_STRING,
                                               NULL,
                                               NULL,
                                               NULL );
            if ( png_ptr != NULL )
            {
                info_ptr = png_create_info_struct( png_ptr );
                if ( info_ptr != NULL )
                {
                    if ( setjmp( png_jmpbuf( (png_ptr) ) ) == 0 )
                    {
                        char tmpstr[1024] = {0,};

                        int png_c_type = PNG_COLOR_TYPE_RGB;

                        if ( img->d() == 4 )
                        {
                            png_c_type = PNG_COLOR_TYPE_RGBA;
                        }

                        png_init_io( png_ptr, fp );
                        // Let compress image with libz
						//  changing compress levle 8 to 7 for speed.
                        png_set_compression_level( png_ptr, 7 );
                        png_set_IHDR( png_ptr,
                                      info_ptr,
                                      img->w(),
                                      img->h(),
                                      8,
                                      png_c_type,
                                      PNG_INTERLACE_NONE,
                                      PNG_COMPRESSION_TYPE_BASE,
                                      PNG_FILTER_TYPE_BASE);

                        png_write_info( png_ptr, info_ptr );

                        const uchar* buff = (const uchar*)img->data()[0];

                        unsigned rowsz = img->w() * sizeof( png_byte ) * img->d();

                        row = (png_bytep)malloc( rowsz );
                        if ( row != NULL )
                        {
                            int bque = 0;
                            int minur = img->h() / 50;

                            for( int y=0; y<img->h(); y++ )
                            {
                                memcpy( row, &buff[bque], rowsz );
                                bque += rowsz;

                                png_write_row( png_ptr, row );

                                // Display progress..
                                if ( ( (y%minur) == 0 ) || ( y == img->h() ) )
                                {
                                    sprintf( tmpstr, "Saving in progress ... ( %.2f %% )",
                                             (float)y / (float)img->h() * 100.f );
                                    window->label( tmpstr );
#ifndef _WIN32
                                    Fl::check();
#endif /// of _WIN32
                                }
                            }

                            png_write_end( png_ptr, NULL );

                            fclose( fp );
                            free(row);

                            window->label( wintitlestr );
                        }

                        png_free_data(png_ptr, info_ptr, PNG_FREE_ALL, -1);
                        png_destroy_write_struct(&png_ptr, (png_infopp)NULL);

                        return true;
                    }
                    png_free_data(png_ptr, info_ptr, PNG_FREE_ALL, -1);
                }
                png_destroy_write_struct(&png_ptr, (png_infopp)NULL);
            }
        }
    }

    return false;
}
