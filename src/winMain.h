#ifndef __WINMAIN_H__
#define __WINMAIN_H__

#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Image.H>
#include <FL/Fl_RGB_Image.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Hor_Fill_Slider.H>

#include "Fl_ImageViewer.H"

#include <vector>
#include <string>

class WMain : public Fl_ImageViewerNotifier
{
    public:
        WMain( int argc, char** argv );
        ~WMain();

    public:
        int Run();

    public:
        void WidgetCall( Fl_Widget* w );
        static void WidgetCB( Fl_Widget* w, void* p )
        {
            if ( p != NULL )
            {
                WMain* pwm = (WMain*)p;
                pwm->WidgetCall( w );
            }
        }
        void* PThreadCall();
        static void* PThreadCB( void* p )
        {
            if ( p != NULL )
            {
                WMain* pwm = (WMain*)p;
                return pwm->PThreadCall();
            }
        }

    private:
        void setdefaultwintitle();
        void createComponents();
        void getEnvironments();
        void startMyImageScan();
        void updateCrossFadeBG();
        int sensFitting( const Fl_RGB_Image* img );
        // int  testImageFile();
        // result : <0 = unknown, 1=JPG, 2=PNG, 3=BMP ...
        int  testImageFile( const wchar_t* imgfp = NULL, char** buff = NULL, size_t* buffsz = NULL );
        int  testImageFile( const char* imgfp = NULL, char** buff = NULL,size_t* buffsz = NULL );
        bool convImage( Fl_RGB_Image* src, Fl_RGB_Image* &dst );
        void saveImageProc();
        bool save2png( Fl_RGB_Image* img, const char* fname );

    private:
        int     _argc;
        char**  _argv;

    private:
        float       crossfade_ratio;
        unsigned    crossfade_idx;

    protected: /// inherit to Fl_ImageViewerNotifier
        void OnMouseMove( void* w, int x, int y, bool inside );
        void OnMouseClick( void* w, int x, int y, unsigned btn );
        void OnKeyPressed( void* w, unsigned short k, int s, int c, int a );
        void OnDropFiles( void* w, char* files );
        void OnDrawCompleted();

    protected:
        Fl_Double_Window*   window;
        Fl_Group*           grpControl;
        Fl_Group*           grpControlEmpty;
        Fl_Box*             boxControl;
        Fl_Hor_Fill_Slider* sliBlur;
        Fl_Hor_Fill_Slider* sliCont;
        Fl_Button*          btnSave;
        Fl_Group*           grpImageView;
        Fl_ImageViewer*     boxImgPreView;
        Fl_RGB_Image*       imgControlBG;
        Fl_RGB_Image*       imgSource;
        Fl_RGB_Image*       imgBlurred;

    private:
        #define             MAX_WINTITLE_LEN        512
        char wintitlestr[MAX_WINTITLE_LEN];

    protected:
        std::wstring        pathHome;
        std::wstring        pathSystemBase;
        std::wstring        pathUserData;
        std::wstring        pathUserRoaming;

    protected:
#ifdef SUPPORT_WCHAR
        std::wstring        pathImage;
        std::wstring        imgFName;
#else
        std::string         pathImage;
        std::string         imgFName;
#endif
        std::string         imgFNameUTF8;

    protected:
        std::vector< std::wstring > listBackImages;
};

#endif // __WINMAIN_H__
