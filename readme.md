# Make Images O' Blurred

```
*WARNING*
This software is still in progress of development.
```

## Require to build source.
1. Latest version of MinGW-W64 or Mac OS X ( El capitan ) with llvm-gcc
1. Lastest version of fl_imgtk (debug & sse3(openmp option) )
1. Latest version of fltk-1.3.4-2-ts

## Updates in latest version 
* Version 0.1.3.8
    1. Improved to loading grayscale image to RGB.

## Previous updates
* Version 0.1.2.6
    1. Fixed a bug : don't remove blurred image when new image source loaded.

* Version 0.1.2.5
    1. Prototype commited.
    1. Using OpenMP, and currently build for Windows.

## Supported OS
1. Windows NT 6.1 or above
